<?php

function deadtome_record_list($account) {
  $destination = drupal_get_destination();

  // Set up the header.
  $header = array(
    'user' => array('data' => t('User')),
    'operations' => array('data' => t('Operations')),
  );

  $dtmids = db_query("SELECT dtmid FROM {deadtome} WHERE uid = :uid", array(':uid' => $account->uid))->fetchCol();
  $records = deadtome_load_multiple($dtmids);
  $dead_accounts = user_load_multiple($account->deadtome);

  $rows = array();
  foreach ($records as $record) {
    $row = array();
    $row[] = format_username($dead_accounts[$record->dead_uid]);
    $row[] = l(t('Undead'), "user/{$account->uid}/blocklist/remove/{$record->dtmid}");
    $rows[] = $row;
  }

  $page['records'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No users are dead to you.') . ' ' . l(t('Add dead user.'), "user/{$account->uid}/blocklist/add", array('query' => $destination)),
  );
  return $page;
}

function deadtome_record_add_form($form, $form_state, $account) {
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );
  $form['dead_uid'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  $form['dead_account'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Hide which user?'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => '',
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : "user/{$account->uid}/blocklist",
  );

  return $form;
}

function deadtome_record_add_form_validate($form, &$form_state) {
  $dead_account = user_load_by_name($form_state['values']['name']);

  if (empty($dead_account)) {
    form_set_error('name', t('User not found.'));
  }
  elseif ($dead_account->uid == $form_state['values']['uid']) {
    form_set_error('name', t('You cannot declare yourself dead.'));
  }
  elseif (db_query("SELECT 1 FROM {deadtome} WHERE uid = :uid AND dead_uid = :deaduid", array(':uid' => $form_state['values']['uid'], ':deaduid' => $dead_account->uid))->fetchField()) {
    form_set_error('name', t('You already have this user declared dead.'));
  }
  else {
    // Set the dead user's UID value back into the form.
    $form_state['values']['dead_uid'] = $dead_account->uid;
    $form_state['values']['dead_account'] = $dead_account;
  }
}

function deadtome_record_add_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $record = (object) $form_state['values'];
  deadtome_save($record);
  drupal_set_message(t('%username is now dead to you.', array('%username' => format_username($form_state['values']['dead_account']))));
  $form_state['redirect'] = "user/{$record->uid}/blocklist";
}

function deadtome_record_remove_form($form, &$form_state, $record) {
  $form['dtmid'] = array(
    '#type' => 'value',
    '#value' => $record->dtmid,
  );
  $form['#record'] = $record;
  $form['#dead_account'] = user_load($record->dead_uid);

  return confirm_form(
    $form,
    t('Are you sure you want to un-declare %username as dead?', array('%username' => format_username($form['#dead_account']))),
    "user/{$record->uid}/blocklist"
  );
}

function deadtome_record_remove_form_submit($form, &$form_state) {
  $record = $form['#record'];
  deadtome_delete($form_state['values']['dtmid']);
  drupal_set_message(t('%username is now alive to you.', array('%username' => format_username($form['#dead_account']))));
  $form_state['redirect'] = "user/{$record->uid}/blocklist";
}
